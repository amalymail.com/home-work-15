﻿#include <iostream>

void function_bla(int N, bool TypeP) {
    std::cout << "\n" << 0 << "\n";
    for (int i = 1 + !TypeP; i <= N; i += 2)
        std::cout << i << "\n";
}

void main() {
    function_bla(10, 0);
    function_bla(12, 1);

    int blabla;
    std::cin >> blabla;
}